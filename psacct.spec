Name:             psacct
Version:          6.6.4
Release:          5
Summary:          Utilities for monitoring process activities
License:          GPLv3+
URL:              http://www.gnu.org/software/acct/
Source:           ftp://ftp.gnu.org/pub/gnu/acct/acct-%{version}.tar.gz
Source1:          psacct.service
Source2:          psacct-logrotate.in
Patch0001:        psacct-6.6.1-SEGV-when-record-incomplete.patch
BuildRequires:    autoconf systemd gcc
Recommends:       %{name}-help = %{version}-%{release}
Requires:         info coreutils
Requires(post):   chkconfig systemd-sysv systemd
Requires(preun):  systemd
Requires(postun): systemd
Conflicts:        filesystem < 3

%description
The psacct package contains several utilities for monitoring process
activities, including ac, lastcomm, accton and sa:
The ac command displays statistics about how long users have been logged on.
The lastcomm command displays information about previous executed commands.
The accton command turns process accounting on or off.
The sa command summarizes information about previously executed commands.

%package        help
Summary:        Help documents for psacct

%description    help
The psacct-help package conatins manual pages and other related files for psacct.

%prep
%autosetup -n acct-%{version} -p1

sed -i 's|.*(gets,.*||g' lib/stdio.in.h
sed -i 's|@ACCT_FILE_LOC@|/var/account/pacct|g'      files.h.in
sed -i 's|@SAVACCT_FILE_LOC@|/var/account/savacct|g' files.h.in
sed -i 's|@USRACCT_FILE_LOC@|/var/account/usracct|g' files.h.in

%build
%configure --enable-linux-multiformat

make

%install
install -d %{buildroot}{/sbin,%{_bindir},%{_mandir},%{_sbindir}}
make install prefix=%{buildroot}%{_prefix} \
        bindir=%{buildroot}%{_bindir} sbindir=%{buildroot}%{_sbindir} \
        infodir=%{buildroot}%{_datadir}/info mandir=%{buildroot}%{_mandir}
cp dump-acct.8 %{buildroot}%{_mandir}/man8/

install -d %{buildroot}/var/account
touch %{buildroot}/var/account/pacct && chmod 0600 %{buildroot}/var/account/pacct

install -d %{buildroot}/etc/logrotate.d
sed -e 's|%%{_bindir}|%{_bindir}|g' -e 's|%%{_sbindir}|%{_sbindir}|g' %{SOURCE2} > %{buildroot}/etc/logrotate.d/psacct

install -d %{buildroot}%{_unitdir}
install -m644 %{SOURCE1} %{buildroot}%{_unitdir}

%post
%systemd_post psacct.service

/sbin/install-info %{_infodir}/accounting.info %{_infodir}/dir || :
touch /var/account/pacct && chmod 0600 /var/account/pacct

%preun
%systemd_preun psacct.service

if [ $1 -eq 0 ]; then
    /sbin/install-info --delete %{_infodir}/accounting.info %{_infodir}/dir || :
fi

%postun
%systemd_postun_with_restart psacct.service

%triggerun -- psacct < 6.5.5-3
%{_bindir}/systemd-sysv-convert --save psacct >/dev/null 2>&1 ||:
/sbin/chkconfig --del psacct >/dev/null 2>&1 || :
/bin/systemctl try-restart psacct.service >/dev/null 2>&1 || :

%files
%doc README COPYING
%dir /var/account
%{_bindir}/{ac,lastcomm}
%{_unitdir}/psacct.service
%{_infodir}/accounting.info.gz
%{_sbindir}/{accton,sa,dump-utmp,dump-acct}
%attr(0600,root,root)  %ghost %config /var/account/pacct
%attr(0644,root,root)  %config(noreplace) /etc/logrotate.d/*
%exclude %{_bindir}/last
%exclude %{_infodir}/dir

%files help
%{_mandir}/man1/*
%{_mandir}/man8/*
%exclude %{_mandir}/man1/last.1*

%changelog
* Thu Nov 12 2020 xinghe <xinghe1@huawei.com> - 6.6.4-5
- add help for Recommends

* Tue Jan 21 2020 daiqianwen <daiqianwen@huawei.com> - 6.6.4-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: modify spec

* Wed Nov 06 2019 yanzhihua <yanzhihua4@huawei.com> - 6.6.4-3
- Package init

